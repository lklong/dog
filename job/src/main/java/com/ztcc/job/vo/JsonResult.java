package com.ztcc.job.vo;

import lombok.Data;

/**
 * <p/>
 * <li>Description:JsonResult</li>
 * <li>@author: liukailong <kailong.liu@cdcalabar.com> </li>
 * <li>Date: 18-4-1 下午4:03</li>
 * <li>@version: 2.0.0 </li>
 * <li>@since JDK 1.8 </li>
 */
@Data
public class JsonResult {

    private int code;
    private String info;
    private Object data;


}
