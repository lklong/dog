package com.ztcc.job.vo;

import com.ztcc.job.entity.Work;
import lombok.Data;

/**
 * <p/>
 * <li>Description:WorkVO</li>
 * <li>@author: liukailong <kailong.liu@cdcalabar.com> </li>
 * <li>Date: 18-4-1 上午12:12</li>
 * <li>@version: 2.0.0 </li>
 * <li>@since JDK 1.8 </li>
 */
@Data
public class WorkVO extends Work {
    private String webName;
}
