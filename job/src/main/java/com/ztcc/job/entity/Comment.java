package com.ztcc.job.entity;

import java.util.Date;

public class Comment {
    private String commer;

    private Date commTime;

    private Integer companyId;

    private Integer commWebId;

    private Long score;

    private Long jobId;

    private String commContent;

    public String getCommer() {
        return commer;
    }

    public void setCommer(String commer) {
        this.commer = commer == null ? null : commer.trim();
    }

    public Date getCommTime() {
        return commTime;
    }

    public void setCommTime(Date commTime) {
        this.commTime = commTime;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getCommWebId() {
        return commWebId;
    }

    public void setCommWebId(Integer commWebId) {
        this.commWebId = commWebId;
    }

    public Long getScore() {
        return score;
    }

    public void setScore(Long score) {
        this.score = score;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public String getCommContent() {
        return commContent;
    }

    public void setCommContent(String commContent) {
        this.commContent = commContent == null ? null : commContent.trim();
    }
}