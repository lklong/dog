package com.ztcc.job.entity;

public class ZhiYou {
    private Long id;

    private String companyName;

    private String zhiYouUrl;

    private Long zhiYouScor;

    private String companyIntro;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    public String getZhiYouUrl() {
        return zhiYouUrl;
    }

    public void setZhiYouUrl(String zhiYouUrl) {
        this.zhiYouUrl = zhiYouUrl == null ? null : zhiYouUrl.trim();
    }

    public Long getZhiYouScor() {
        return zhiYouScor;
    }

    public void setZhiYouScor(Long zhiYouScor) {
        this.zhiYouScor = zhiYouScor;
    }

    public String getCompanyIntro() {
        return companyIntro;
    }

    public void setCompanyIntro(String companyIntro) {
        this.companyIntro = companyIntro == null ? null : companyIntro.trim();
    }
}