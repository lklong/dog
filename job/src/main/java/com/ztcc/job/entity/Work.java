package com.ztcc.job.entity;

import java.util.Date;

public class Work {
    private Long jobId;

    private String jobName;

    private String salary;

    private Integer eduBackgr;

    private String keyWord;

    private String companyName;

    private Integer webId;

    private Long jobScore;

    private Long kanZhunScore;

    private Long zhiYouScore;

    private String kanZhunUrl;

    private String zhiYouUrl;

    private Long orgId;

    private Date workPosition;

    private Date pubTime;

    private Date lastUpdTime;

    private Integer cityId;

    private String companyIntro;

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName == null ? null : jobName.trim();
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary == null ? null : salary.trim();
    }

    public Integer getEduBackgr() {
        return eduBackgr;
    }

    public void setEduBackgr(Integer eduBackgr) {
        this.eduBackgr = eduBackgr;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord == null ? null : keyWord.trim();
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    public Integer getWebId() {
        return webId;
    }

    public void setWebId(Integer webId) {
        this.webId = webId;
    }

    public Long getJobScore() {
        return jobScore;
    }

    public void setJobScore(Long jobScore) {
        this.jobScore = jobScore;
    }

    public Long getKanZhunScore() {
        return kanZhunScore;
    }

    public void setKanZhunScore(Long kanZhunScore) {
        this.kanZhunScore = kanZhunScore;
    }

    public Long getZhiYouScore() {
        return zhiYouScore;
    }

    public void setZhiYouScore(Long zhiYouScore) {
        this.zhiYouScore = zhiYouScore;
    }

    public String getKanZhunUrl() {
        return kanZhunUrl;
    }

    public void setKanZhunUrl(String kanZhunUrl) {
        this.kanZhunUrl = kanZhunUrl == null ? null : kanZhunUrl.trim();
    }

    public String getZhiYouUrl() {
        return zhiYouUrl;
    }

    public void setZhiYouUrl(String zhiYouUrl) {
        this.zhiYouUrl = zhiYouUrl == null ? null : zhiYouUrl.trim();
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Date getWorkPosition() {
        return workPosition;
    }

    public void setWorkPosition(Date workPosition) {
        this.workPosition = workPosition;
    }

    public Date getPubTime() {
        return pubTime;
    }

    public void setPubTime(Date pubTime) {
        this.pubTime = pubTime;
    }

    public Date getLastUpdTime() {
        return lastUpdTime;
    }

    public void setLastUpdTime(Date lastUpdTime) {
        this.lastUpdTime = lastUpdTime;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getCompanyIntro() {
        return companyIntro;
    }

    public void setCompanyIntro(String companyIntro) {
        this.companyIntro = companyIntro == null ? null : companyIntro.trim();
    }
}