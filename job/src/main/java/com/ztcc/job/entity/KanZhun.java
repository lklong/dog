package com.ztcc.job.entity;

public class KanZhun {
    private Long id;

    private String companyName;

    private String kanZhunUrl;

    private Long kanZhunScor;

    private String companyIntro;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    public String getKanZhunUrl() {
        return kanZhunUrl;
    }

    public void setKanZhunUrl(String kanZhunUrl) {
        this.kanZhunUrl = kanZhunUrl == null ? null : kanZhunUrl.trim();
    }

    public Long getKanZhunScor() {
        return kanZhunScor;
    }

    public void setKanZhunScor(Long kanZhunScor) {
        this.kanZhunScor = kanZhunScor;
    }

    public String getCompanyIntro() {
        return companyIntro;
    }

    public void setCompanyIntro(String companyIntro) {
        this.companyIntro = companyIntro == null ? null : companyIntro.trim();
    }
}