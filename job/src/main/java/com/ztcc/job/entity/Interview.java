package com.ztcc.job.entity;

import java.util.Date;

public class Interview {
    private String commer;

    private Date commTime;

    private Integer companyId;

    private String jobPosition;

    private String commContent;

    public String getCommer() {
        return commer;
    }

    public void setCommer(String commer) {
        this.commer = commer == null ? null : commer.trim();
    }

    public Date getCommTime() {
        return commTime;
    }

    public void setCommTime(Date commTime) {
        this.commTime = commTime;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition == null ? null : jobPosition.trim();
    }

    public String getCommContent() {
        return commContent;
    }

    public void setCommContent(String commContent) {
        this.commContent = commContent == null ? null : commContent.trim();
    }
}