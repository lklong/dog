package com.ztcc.job.mapper;

import com.ztcc.job.entity.JobCollection;
import com.ztcc.job.entity.JobCollectionExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobCollectionMapper {
    long countByExample(JobCollectionExample example);

    int deleteByExample(JobCollectionExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(JobCollection record);

    int insertSelective(JobCollection record);

    List<JobCollection> selectByExample(JobCollectionExample example);

    JobCollection selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") JobCollection record, @Param("example") JobCollectionExample example);

    int updateByExample(@Param("record") JobCollection record, @Param("example") JobCollectionExample example);

    int updateByPrimaryKeySelective(JobCollection record);

    int updateByPrimaryKey(JobCollection record);

    JobCollection selectByJobId(Integer jobId);
}