package com.ztcc.job.mapper;

import com.ztcc.job.entity.ZhiYou;

public interface ZhiYouMapper {
    int insert(ZhiYou record);

    int insertSelective(ZhiYou record);
}