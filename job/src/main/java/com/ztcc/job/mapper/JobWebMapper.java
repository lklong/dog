package com.ztcc.job.mapper;

import com.ztcc.job.entity.JobWeb;
import org.springframework.stereotype.Repository;

@Repository
public interface JobWebMapper {
    int insert(JobWeb record);

    int insertSelective(JobWeb record);
}