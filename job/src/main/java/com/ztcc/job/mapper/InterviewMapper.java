package com.ztcc.job.mapper;

import com.ztcc.job.entity.Interview;

public interface InterviewMapper {
    int insert(Interview record);

    int insertSelective(Interview record);
}