package com.ztcc.job.mapper;

import com.ztcc.job.entity.Work;
import com.ztcc.job.vo.WorkVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WorkMapper {
    int deleteByPrimaryKey(Long jobId);

    int insert(Work record);

    int insertSelective(Work record);

    Work selectByPrimaryKey(Long jobId);

    int updateByPrimaryKeySelective(Work record);

    int updateByPrimaryKeyWithBLOBs(Work record);

    int updateByPrimaryKey(Work record);

    List<Work> selectByPage(@Param("start") int start, @Param("nums") int nums);

    List<WorkVO> selectByPage2(@Param("start") int start, @Param("nums") int nums);


    List<WorkVO> selectCollectionByPage(int i, int i1);
}