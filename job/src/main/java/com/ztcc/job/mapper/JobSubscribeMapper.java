package com.ztcc.job.mapper;

import com.ztcc.job.entity.JobSubscribe;
import com.ztcc.job.entity.JobSubscribeExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JobSubscribeMapper {
    long countByExample(JobSubscribeExample example);

    int deleteByExample(JobSubscribeExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(JobSubscribe record);

    int insertSelective(JobSubscribe record);

    List<JobSubscribe> selectByExample(JobSubscribeExample example);

    JobSubscribe selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") JobSubscribe record, @Param("example") JobSubscribeExample example);

    int updateByExample(@Param("record") JobSubscribe record, @Param("example") JobSubscribeExample example);

    int updateByPrimaryKeySelective(JobSubscribe record);

    int updateByPrimaryKey(JobSubscribe record);
}