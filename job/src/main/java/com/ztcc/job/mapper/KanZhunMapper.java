package com.ztcc.job.mapper;

import com.ztcc.job.entity.KanZhun;

public interface KanZhunMapper {
    int insert(KanZhun record);

    int insertSelective(KanZhun record);
}