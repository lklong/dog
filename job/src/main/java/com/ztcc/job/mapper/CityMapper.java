package com.ztcc.job.mapper;

import com.ztcc.job.entity.City;

public interface CityMapper {
    int insert(City record);

    int insertSelective(City record);
}