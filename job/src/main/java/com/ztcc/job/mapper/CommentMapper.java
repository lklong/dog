package com.ztcc.job.mapper;

import com.ztcc.job.entity.Comment;

public interface CommentMapper {
    int insert(Comment record);

    int insertSelective(Comment record);
}