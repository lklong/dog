package com.ztcc.job.controller.rest;

import com.ztcc.job.entity.JobCollection;
import com.ztcc.job.service.JobCollectionService;
import com.ztcc.job.vo.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p/>
 * <li>Description:JobCollectionController</li>
 * <li>@author: liukailong <kailong.liu@cdcalabar.com> </li>
 * <li>Date: 18-4-1 上午11:39</li>
 * <li>@version: 2.0.0 </li>
 * <li>@since JDK 1.8 </li>
 */
@RestController
public class JobCollectionRestController {
    @Autowired
    private JobCollectionService jobCollectionService;

    @RequestMapping("/collection/add/{jobId}")
    public JsonResult add(@PathVariable("jobId") int jobId) {
        System.out.println("收藏参数：" + jobId);
        JsonResult jsonResult = new JsonResult();
        JobCollection jobCollection = jobCollectionService.selectByJobId(jobId);
        if (jobCollection == null) {
            jobCollection = new JobCollection();
            jobCollection.setJobId(jobId);
            int res = jobCollectionService.insertSelective(jobCollection);
            if (res == 0) {
                jsonResult.setCode(1000);
                jsonResult.setInfo("添加收藏失败！");
            } else {
                jsonResult.setCode(9999);
                jsonResult.setInfo("添加收藏成功！");
            }
        } else {
            jsonResult.setCode(1001);
            jsonResult.setInfo("已添加过收藏！");
        }
        return jsonResult;
    }

    @RequestMapping("/collection/{jobId}")
    public JsonResult query(@PathVariable("jobId") int jobId) {
        JsonResult jsonResult = new JsonResult();
        JobCollection jobCollection = jobCollectionService.selectByJobId(jobId);
        jsonResult.setData(jobCollection);
        return jsonResult;
    }

}
