package com.ztcc.job.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p/>
 * <li>Description:IndexController</li>
 * <li>@author: liukailong <kailong.liu@cdcalabar.com> </li>
 * <li>Date: 18-3-25 下午5:31</li>
 * <li>@version: 2.0.0 </li>
 * <li>@since JDK 1.8 </li>
 */
@Controller
public class IndexController {

    @RequestMapping("/")
    public String index(){
        return "index";
    }
}
