package com.ztcc.job.controller.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p/>
 * <li>Description:AddController</li>
 * <li>@author: liukailong <kailong.liu@cdcalabar.com> </li>
 * <li>Date: 18-3-25 下午12:34</li>
 * <li>@version: 2.0.0 </li>
 * <li>@since JDK 1.8 </li>
 */
@Controller
public class WebController {

    @RequestMapping("/add")
    public String add(){
        return "add/index";
    }

    @RequestMapping("/addcollection")
    public String addCollection(){
        return "addcollection/index";
    }

//    @RequestMapping("/collection")
//    public String collection(){
//        return "collection/index";
//    }

//    @RequestMapping("/detail")
//    public String detail(){
//        return "detail/index";
//    }

//    @RequestMapping("/worklist")
//    public String listWork(){
//
//        return "worklist/index";
//    }


}
