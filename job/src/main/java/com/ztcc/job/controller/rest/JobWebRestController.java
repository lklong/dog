package com.ztcc.job.controller.rest;

import com.ztcc.job.entity.JobWeb;
import com.ztcc.job.service.JobWebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p/>
 * <li>Description:JobWebController</li>
 * <li>@author: liukailong <kailong.liu@cdcalabar.com> </li>
 * <li>Date: 18-3-25 下午9:47</li>
 * <li>@version: 2.0.0 </li>
 * <li>@since JDK 1.8 </li>
 */
@RestController
public class JobWebRestController {

    @Autowired
    private JobWebService jobWebService;

    @RequestMapping("/jobweb")
    public List<JobWeb> selectJobWeb(){
        return jobWebService.selectJobWeb();
    }
}
