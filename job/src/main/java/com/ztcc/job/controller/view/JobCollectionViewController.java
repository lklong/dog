package com.ztcc.job.controller.view;

import com.ztcc.job.service.JobCollectionService;
import com.ztcc.job.service.WorkService;
import com.ztcc.job.vo.WorkVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * <p/>
 * <li>Description:JobCollectionController</li>
 * <li>@author: liukailong <kailong.liu@cdcalabar.com> </li>
 * <li>Date: 18-4-1 上午11:39</li>
 * <li>@version: 2.0.0 </li>
 * <li>@since JDK 1.8 </li>
 */
@Controller
public class JobCollectionViewController {
    @Autowired
    private JobCollectionService jobCollectionService;

    @Autowired
    private WorkService workService;


    @RequestMapping("/collection/list")
    public String list(Model model) {
        List<WorkVO> works = workService.selectCollectionByPage(1, 10);
        model.addAttribute("works", works);
        return "collection/list";
    }
}
