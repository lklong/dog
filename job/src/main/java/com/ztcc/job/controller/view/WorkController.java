package com.ztcc.job.controller.view;

import com.ztcc.job.entity.Work;
import com.ztcc.job.service.WorkService;
import com.ztcc.job.vo.WorkVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * <p/>
 * <li>Description:WorkController</li>
 * <li>@author: liukailong <kailong.liu@cdcalabar.com> </li>
 * <li>Date: 18-3-25 下午8:42</li>
 * <li>@version: 2.0.0 </li>
 * <li>@since JDK 1.8 </li>
 */
@Controller
public class WorkController {

    @Autowired
    private WorkService workService;

    @RequestMapping("/work/detail/{id}")
    public String selectWorkById(@PathVariable(value = "id") long id,
                                 Model model) {
        Work work = workService.selectByPrimaryKey(id);
        model.addAttribute("work", work);
        return "detail/index";
    }

    @RequestMapping("/worklist")
    public String selectWorkByPage(Model model) {
        List<WorkVO> works = workService.selectByPage2(1, 10);
        model.addAttribute("works", works);
        return "worklist/index";
    }
}
