package com.ztcc.job.service.impl;

import com.ztcc.job.entity.JobCollection;
import com.ztcc.job.entity.JobCollectionExample;
import com.ztcc.job.mapper.JobCollectionMapper;
import com.ztcc.job.service.JobCollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p/>
 * <li>Description:JobCollectionServiceImpl</li>
 * <li>@author: liukailong <kailong.liu@cdcalabar.com> </li>
 * <li>Date: 18-4-1 下午4:10</li>
 * <li>@version: 2.0.0 </li>
 * <li>@since JDK 1.8 </li>
 */
@Service
public class JobCollectionServiceImpl implements JobCollectionService {

    @Autowired
    private JobCollectionMapper jobCollectionMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return jobCollectionMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(JobCollection record) {
        return jobCollectionMapper.insert(record);
    }

    @Override
    public int insertSelective(JobCollection record) {
        return jobCollectionMapper.insertSelective(record);
    }

    @Override
    public List<JobCollection> selectByExample(JobCollectionExample example) {
        return jobCollectionMapper.selectByExample(example);
    }

    @Override
    public JobCollection selectByPrimaryKey(Integer id) {
        return jobCollectionMapper.selectByPrimaryKey(id);
    }

    @Override
    public JobCollection selectByJobId(Integer jobId) {
        return jobCollectionMapper.selectByJobId(jobId);
    }

    @Override
    public int updateByPrimaryKeySelective(JobCollection record) {
        return jobCollectionMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(JobCollection record) {
        return jobCollectionMapper.updateByPrimaryKey(record);
    }
}
