package com.ztcc.job.service;

import com.ztcc.job.entity.JobCollection;
import com.ztcc.job.entity.JobCollectionExample;

import java.util.List;

public interface JobCollectionService {

    int deleteByPrimaryKey(Integer id);

    int insert(JobCollection record);

    int insertSelective(JobCollection record);

    List<JobCollection> selectByExample(JobCollectionExample example);

    JobCollection selectByPrimaryKey(Integer id);

    JobCollection selectByJobId(Integer jobId);

    int updateByPrimaryKeySelective(JobCollection record);

    int updateByPrimaryKey(JobCollection record);
}