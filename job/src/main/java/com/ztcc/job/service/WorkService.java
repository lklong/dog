package com.ztcc.job.service;

import com.ztcc.job.entity.Work;
import com.ztcc.job.vo.WorkVO;

import java.util.List;

public interface WorkService {
    int deleteByPrimaryKey(Long jobId);

    int insert(Work record);

    int insertSelective(Work record);

    Work selectByPrimaryKey(Long jobId);

    List<Work> selectByPage(int start, int nums);

    List<WorkVO> selectByPage2(int start, int nums);

    int updateByPrimaryKeySelective(Work record);

    int updateByPrimaryKeyWithBLOBs(Work record);

    int updateByPrimaryKey(Work record);

    List<WorkVO> selectCollectionByPage(int i, int i1);
}