package com.ztcc.job.service;

import com.ztcc.job.entity.JobWeb;

import java.util.List;

/**
 * <p/>
 * <li>Description:JobWebService</li>
 * <li>@author: liukailong <kailong.liu@cdcalabar.com> </li>
 * <li>Date: 18-3-25 下午9:47</li>
 * <li>@version: 2.0.0 </li>
 * <li>@since JDK 1.8 </li>
 */
public interface JobWebService {

    int insert(JobWeb record);

    int insertSelective(JobWeb record);

    List<JobWeb> selectJobWeb();
}
