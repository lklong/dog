package com.ztcc.job.service.impl;

import com.ztcc.job.entity.JobWeb;
import com.ztcc.job.mapper.JobWebMapper;
import com.ztcc.job.service.JobWebService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p/>
 * <li>Description:JobWebService</li>
 * <li>@author: liukailong <kailong.liu@cdcalabar.com> </li>
 * <li>Date: 18-3-25 下午9:47</li>
 * <li>@version: 2.0.0 </li>
 * <li>@since JDK 1.8 </li>
 */
@Service
public class JobWebServiceImpl implements JobWebService{

    @Resource
    private JobWebMapper jobWebMapper;

    @Override
    public int insert(JobWeb record) {
        return 0;
    }

    @Override
    public int insertSelective(JobWeb record) {
        return 0;
    }

    @Override
    public List<JobWeb> selectJobWeb() {
        return null;
    }
}
