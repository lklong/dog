package com.ztcc.job.service.impl;

import com.ztcc.job.entity.Work;
import com.ztcc.job.mapper.WorkMapper;
import com.ztcc.job.service.WorkService;
import com.ztcc.job.vo.WorkVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class WorkServiceImpl implements WorkService {

    @Resource
    private WorkMapper workMapper;

    @Override
    public int deleteByPrimaryKey(Long jobId) {
        return 0;
    }

    @Override
    public int insert(Work record) {
        return 0;
    }

    @Override
    public int insertSelective(Work record) {
        return 0;
    }

    @Override
    public Work selectByPrimaryKey(Long jobId) {
        return workMapper.selectByPrimaryKey(jobId);
    }

    @Override
    public List<Work> selectByPage(int start, int nums) {
        return workMapper.selectByPage(start, nums);
    }

    @Override
    public List<WorkVO> selectByPage2(int start, int nums) {
        return workMapper.selectByPage2(start, nums);
    }

    @Override
    public int updateByPrimaryKeySelective(Work record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKeyWithBLOBs(Work record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKey(Work record) {
        return 0;
    }

    @Override
    public List<WorkVO> selectCollectionByPage(int i, int i1) {
        return workMapper.selectCollectionByPage(i, i1);
    }
}